let name = prompt('What is your name?');
let socked = new WebSocket('ws://localhost:5001');

if (name === '') {
    name = 'user';
}


let log = document.getElementById('log');

socked.onmessage = function(event) {
    let json = JSON.parse(event.data);
    showOnline(json);
    log.innerHTML += json.name + ': '+ json.data + '<br>';
};

// Click on button will return message with nickname and send in object
document.querySelector('button').onclick = showMessage;

function showMessage() {
    let text = document.getElementById('text').value;
    socked.send(JSON.stringify({
        type: 'name',
        data: name,
    }));

    socked.send(JSON.stringify({
        type: 'message',
        data: text
    }));
    log.innerHTML += 'You: '+ text +'<br>';
}


function showOnline(json) {
    let onlineConteiner = document.getElementById('online');
    let activeUsers = [...onlineConteiner.children];
    activeUsers.forEach((elem) => {
        elem.remove();
    });
    json.onlineUsers.forEach((elem) => {
        let onlineData = document.createElement('li');
        onlineData.append(elem);
        onlineConteiner.append(onlineData);
    })
}

