let server = require('ws').Server;
let s = new server({port: 5001});
let onlineUsers = [];

s.on('connection', function(ws) {
    let id = Math.random();
    onlineUsers[id] = ws;

    ws.on('message', function(message) {

        message = JSON.parse(message);
        if (message.type === 'name') {
            ws.name = message.data;
            let temp = onlineUsers.every(elem => {
                return elem !== ws.name;
            });
            if (temp) {
                onlineUsers.push(ws.name);
            }

            console.log(onlineUsers.length);


            // Get array with clients witch online
            //console.log(onlineUsers.personName);

            // Here we call message with information about nickname, when somebody online
            return console.log(`${ws.name} is online`)
        }

        // Get message in console from wall
        console.log(`${ws.name}: ` + message.data);

        // Show messages for all clients
        s.clients.forEach(function eventFunction(client) {
            if (client !== ws) {
                client.send(JSON.stringify({
                    name: ws.name,
                    data: message.data,
                    onlineUsers: onlineUsers,
                }));
            }
        });
    });

    ws.on('close', () => {
        for (let i = 0; i < onlineUsers.length; i++) {
            if (ws.name === onlineUsers[i]) {
                console.log(`${onlineUsers[i]} leave from chat`);
                onlineUsers.splice(i, 1);
            }
            console.log(onlineUsers.length);
        }
    });
});