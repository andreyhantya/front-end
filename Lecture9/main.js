/*
let canvas = document.getElementById('canvas');
let c = canvas.getContext('2d');

const drawSomeLine = (event) => {
    c.lineTo(event.clientX, event.clientY);
    c.stroke();
};

canvas.addEventListener("mousemove", drawSomeLine);*/


let canvas = document.getElementById('canvas');
let c = canvas.getContext('2d');

canvas.addEventListener("mousedown", drawSomeLine = (event) => {
    c.beginPath();
    canvas.addEventListener("mousemove", draw);
    c.stroke();
});

const draw = (event) => {
    c.lineTo(event.clientX, event.clientY);
    c.stroke();

    canvas.addEventListener("mouseup", function () {
        canvas.removeEventListener("mousemove", draw);
    });
};


canvas.addEventListener("mousedown", drawSomeLine);
