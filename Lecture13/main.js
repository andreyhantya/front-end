/*
const person = {
    _name: 'Andrey',
    _age: 26,
    gender: 'Male',
    _skills: ['JS', 'React'],
    get name() {
        return this._name;
    },
    set name(value) {
        this._name = value;
    },
    get age() {
        return this._age;
    },
    set age(value) {
        if (value > 0 && value < 200) {
            this._age = value;
        } else {
            console.log("Ошибка")
        }
    },
    set skills(value) {
        if (!this._skills.find(skills => skills === value)) {
            this._skills = [...this._skills, value];
        }
    }

};



//Дискрипторы свойств

console.log(Object.getOwnPropertyDescriptor(person, 'gender')); //указываем зис то есть обьект и поле с которым надо работать

//как заменить дескриптор:
Object.defineProperties(person, 'gender', {
    value: 'Male',
    writable: false, // нельзя будет изменить на прямую
    enumerable: true, //не будет учавствовать в итерациях
    configurable: true, //если будет фолс то не сможем менять эти настройки
    //если ниче не укажем то засетить все фолсом
}); //обьект и свойсто в которым хотим работать


console.log(Object.keys(person)); // енумирейбл прячет свойство


person._gender = 'aasas';

Object.defineProperties(person,{
    _age: {
            value: 26,
            writable: false,
            enumerable: false,
            configurable: true,
    },
    age: {
        set(value) {
            Object.defineProperty(person, '_age',{
                value
            })
        },
        get() {
            return this._age;
        }
    }
});

person._age = 12212; //сетим значение
console.log(person.age);
person.age = 2;
console.log(person.age);

person.family = "asdasd";
console.log('qweqwe', person);
Object.preventExtensions(person);//чтобы запрещает изменение обьекта
person.add = ""; // не сработает уже

*/

//                      ИТЕРИРУЕМЫЕ МАССИВЫ

//символ-итератор перебирает елементы и возвращает значения
/*
let skills = ['js', 'java', 'redux'];
for (let skill of skills) {
    console.log('skill', skill);
}*/


/*
//как работает под капотом
return{value, done: false} //если дан фолс по идет дальше по циклу если тру то останавливается цикл
*/
/*
console.log(skills[Symbol.iterator]);
let iterator = skills[Symbol.iterator]();
console.log(iterator.next().value);
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());//donee : true;*/
/*

//Сделать свой гениратор который будет нам просто какой то коуенур
const myGenerator = {
  [Symbol.iterator]() {
      let index = 1;
      return {
          next() {
              let value = index > 10 ? undefined : index++;
              let done = !value;
              return {value, done};
          }
      }
  }
};

for (let gen of myGenerator) {
    console.log('gen', gen);
}
*/

/*
function generate() {
    console.log('start');
    console.log('end');
}
*/

/*
function* generate() {  //звездочка - обозначение генератора можем приостановить ф-ю
        yield 1;
        yield 2;
        yield 3;
}
let iter = generate();
    console.log(iter.next()); //работает  как счетчик или дебагер
    console.log(iter.next()); //работает  как счетчик или дебагер
    console.log(iter.next()); //работает  как счетчик или дебагер
    console.log(iter.next()); //работает  как счетчик или дебагер
*/

function* generate() {  //звездочка - обозначение генератора можем приостановить ф-ю
        const res = 5 + (yield);
        yield console.log(res);
}
let iter = generate();
    console.log(iter.next()); //работает  как счетчик или дебагер
    console.log(iter.next(3)); //работает  как счетчик или дебагер
    console.log(iter.next()); //работает  как счетчик или дебагер
    console.log(iter.next()); //работает  как счетчик или дебагер
