describe("HomeWork2 tests function", function () {
    describe('getType', () => {
        const testData = [
            {
                a: 2,
                expected: "number",
            },
            {
                a: "Hello world",
                expected: "string",
            },
            {
                a: true,
                expected: "boolean",
            },
            {
                a: null,
                expected: "object",
            },
            {
                a: undefined,
                expected: "undefined",

            }
        ];

        testData.forEach(data => {
            const {a, expected} = data;

            const actual = getType(a);

            it(`Вводим:  ${a} Должно вернуть:  ${expected}`, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });


    describe('getWholeString', () => {
        const testData = [
            {
                one: 2,
                two: undefined,
                three: "World",
                expected: "2World",
            },
            {
                one: 2,
                two: undefined,
                three: 6,
                expected: "26",
            },
            {
                one: "Hello",
                two: 0,
                three: undefined,
                expected: "Hello0",
            },
            {
                one: 2,
                two: "Hello",
                three: "",
                expected: "2Hello",
            }
        ];

        testData.forEach(data => {
            const {one, two, three, expected} = data;

            const actual = getWholeString(one,two,three);

            it(`Вводим: one = ${one}, two = ${one}, three = ${one} Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });



    describe('countFactorial', () => {
        const testData = [
            {
                n: 3,
                expected: 6,
            },
            {
                n: 1,
                expected: 1,
            },
            {
                n: 0,
                expected: 1,
            },
            {
                n: -5,
                expected: "Введите положительное число",
            },
            {
                 n: 7,
                 expected: 5040,
             }
        ];

        testData.forEach(data => {
            const {n, expected} = data;

            const actual = countFactorial(n);

            it(`Вводим:  ${n} Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });

    describe('getEven', () => {
        const testData = [
            {
                a: 2,
                b: -5,
                expected: -10,
            },
            {
                a: 5,
                b: 2,
                expected: 7,
            },
            {
                a: -5,
                b: -5,
                expected: -10,
            },
            {
                a: 2,
                b: 7,
                expected: 14,
            }
        ];

        testData.forEach(data => {
            const {a, b, expected} = data;

            const actual = getEven(a,b);

            it(`Вводим: a = ${a}, b = ${b} Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });


    describe('coordinates', () => {
        const testData = [
            {
                a: 2,
                b: 4,
                expected: 1,
            },
            {
                a: 0,
                b: 0,
                expected: 0,
            },
            {
                a: 0,
                b: 2,
                expected: 0,
            },
            {
                a: -4,
                b: 2,
                expected: 2,
            }
        ];

        testData.forEach(data => {
            const {a, b, expected} = data;

            const actual = coordinates(a,b);

            it(`Вводим: a = "${a}", b = ${b} Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });

    describe('positiveNumber', () => {
        const testData = [
            {
                a: 2,
                b: 4,
                c: 0,
                expected: 6,
            },
            {
                a: -5,
                b: 4,
                c: 1,
                expected: 5,
            },
            {
                a: -5,
                b: -4,
                c: 1,
                expected: 1,
            },
            {
                a: 0,
                b: 0,
                c: -5,
                expected: 0,
            }
        ];

        testData.forEach(data => {
            const {a, b, c, expected} = data;

            const actual = positiveNumber(a,b,c);

            it(`Вводим: a = "${a}", b = ${b}, c = ${c} Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });


    describe('maxOfNum', () => {
        const testData = [
            {
                a: 2,
                b: 5,
                c: 1,
                expected: 13,
            },
            {
                a: -5,
                b: 5,
                c: 1,
                expected: 4,
            },
            {
                a: 0,
                b: 0,
                c: 1,
                expected: 4,
            },
            {
                a: 6,
                b: 0,
                c: -5,
                expected: 4,
            }
        ];

        testData.forEach(data => {
            const {a, b, c, expected} = data;

            const actual = maxOfNumb(a,b,c);

            it(`Вводим: a = "${a}", b = ${b}, c = ${c} Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });



    describe('assessment', () => {
        const testData = [
            {
                x: -5,
                expected: "Введите корректный рейтинг!",
            },
            {
                x: 2,
                expected: "F",
            },
            {
                x: 20,
                expected: "E",
            },
            {
                x: 40,
                expected: "D",
            },
            {
                x: 60,
                expected: "C",
            },
            {
                x: 75,
                expected: "B",
            },
            {
                x: 90,
                expected: "A",
            }
        ];

        testData.forEach(data => {
            const {x, expected} = data;

            const actual = assessment(x);

            it(`Вводим:  "${x}", Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });



    describe('sumOfNumber', () => {
        const testData = [
            {
                num: 5,
                expected: "Sum = 6 Quantity = 2",
            },
            {
                num: -5,
                expected: "Введите число от 1 до 99",
            },
            {
                num: 99,
                expected: "Sum = 2450 Quantity = 49",
            }
        ];

        testData.forEach(data => {
            const {num, expected} = data;

            const actual = sumOfNumber(num);

            it(`Вводим:  "${num}", Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });


    describe('factorial', () => {
        const testData = [
            {
                n: 3,
                expected: 6,
            },
            {
                n: -5,
                expected: "Неверное число",
            },
            {
                n: 1,
                expected: 1,
            },
            {
                n: 0,
                expected: 1,
            },
            {
                n: 6,
                expected: 720,
            },
        ];

        testData.forEach(data => {
            const {n, expected} = data;

            const actual = factorial(n);

            it(`Вводим:  "${n}", Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });

    describe('getMinElement', () => {
        const testData = [
            {
                array: [2,5,4],
                expected: 2,
            },
            {
                array: [],
                expected: "Введите хотябы одно число!",
            },
            {
                array: [12],
                expected: 12,
            },
            {
                array: [2,-5,10],
                expected: -5,
            },
            {
                array: [0,5,1],
                expected: 0,
            },
        ];

        testData.forEach(data => {
            const {array, expected} = data;

            const actual = getMinElement(array);

            it(`Вводим:  [${array}], Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });


    describe('getMaxElement', () => {
        const testData = [
            {
                array: [2,5,4],
                expected: 5,
            },
            {
                array: [],
                expected: "Введите хотябы одно число!",
            },
            {
                array: [12],
                expected: 12,
            },
            {
                array: [2,-5,10],
                expected: 10,
            },
            {
                array: [0,5,1],
                expected: 5,
            },
        ];

        testData.forEach(data => {
            const {array, expected} = data;

            const actual = getMaxElement(array);

            it(`Вводим:  [${array}], Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });


    describe('getMinIndexElement', () => {
        const testData = [
            {
                array: [2,5,4],
                expected: 0,
            },
            {
                array: [],
                expected: 'Введите минимум 1 елемент',
            },
            {
                array: [12],
                expected: 0,
            },
            {
                array: [2,-5,10],
                expected: 1,
            },
            {
                array: [0,5,-1],
                expected: 2,
            },
        ];

        testData.forEach(data => {
            const {array, expected} = data;

            const actual = getMinIndexElement(array);

            it(`Вводим:  [${array}], Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });


    describe('getMaxIndexElement', () => {
        const testData = [
            {
                array: [2,5,4],
                expected: 1,
            },
            {
                array: [],
                expected: 'Введите минимум 1 елемент',
            },
            {
                array: [12],
                expected: 0,
            },
            {
                array: [2,-5,10],
                expected: 2,
            },
            {
                array: [0,5,-1],
                expected: 1,
            },
        ];

        testData.forEach(data => {
            const {array, expected} = data;

            const actual = getMaxIndexElement(array);

            it(`Вводим:  [${array}], Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });


    describe('sumIndexElement', () => {
        const testData = [
            {
                array: [2,4,6,7],
                expected: 11,
            },
            {
                array: [],
                expected: 0,
            },
            {
                array: [-5,-4,2,1],
                expected: -3,
            },
            {
                array: [-5,3,0,1],
                expected: 4,
            },
            {
                array: [0,0,0],
                expected: 0,
            },
        ];

        testData.forEach(data => {
            const {array, expected} = data;

            const actual = sumIndexElement(array);

            it(`Вводим:  [${array}], Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });


    describe('getReverce', () => {
        const testData = [
            {
                array: [1,2,3,4],
                expected: [4,3,2,1],
            },
            {
                array: [],
                expected: "Введите минимум 2 элемента",
            },
            {
                array: [1],
                expected: "Введите минимум 2 элемента",
            },
            {
                array: [-5,3,0,1],
                expected: [1,0,3,-5],
            },
            {
                array: [1,2],
                expected: [2,1],
            },
        ];

        testData.forEach(data => {
            const {array, expected} = data;

            const actual = getReverce(array);

            it(`Вводим:  [${array}], Должно вернуть: ${expected} `, () => {
                assert.deepEqual(actual, expected);
            });
        });
    });


    describe('getOddElements', () => {
        const testData = [
            {
                array: [2,5,-5,0],
                expected: 2,
            },
            {
                array: [],
                expected: 0,
            },
            {
                array: [2],
                expected: 0,
            },
            {
                array: [-5,0,-2],
                expected: 1,
            },
            {
                array: [1,-1],
                expected: 2,
            },
        ];

        testData.forEach(data => {
            const {array, expected} = data;

            const actual = getOddElements(array);

            it(`Вводим:  [${array}], Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });

    describe('arrayOfMirror', () => {
        const testData = [
            {
                array: [1,2,3,4],
                expected: [3,4,1,2],
            },
            {
                array: [4],
                expected: "Введите четное кол-во элементов",
            },
            {
                array: [0,2,-5,7],
                expected: [-5,7,0,2],
            },
            {
                array: [-5,0,1,4],
                expected: [1,4,-5,0],
            },
        ];

        testData.forEach(data => {
            const {array, expected} = data;

            const actual = arrayOfMirror(array);

            it(`Вводим:  [${array}], Должно вернуть: ${expected} `, () => {
                assert.deepEqual(actual, expected);
            });
        });
    });

    describe('BubbleSort', () => {
        const testData = [
            {
                array: [1,6,4,2],
                expected: [1,2,4,6],
            },
            {
                array: [4],
                expected: [4],
            },
            {
                array: [0,2,-5,7],
                expected: [-5,0,2,7],
            },
            {
                array: [-5,4,0,-1],
                expected: [-5,-1,0,4],
            },
        ];

        testData.forEach(data => {
            const {array, expected} = data;

            const actual = BubbleSort(array);

            it(`Вводим:  [${array}], Должно вернуть: ${expected} `, () => {
                assert.deepEqual(actual, expected);
            });
        });
    });

    describe('SelectionSort', () => {
        const testData = [
            {
                array: [1,6,4,2],
                expected: [1,2,4,6],
            },
            {
                array: [4],
                expected: [4],
            },
            {
                array: [0,2,-5,7],
                expected: [-5,0,2,7],
            },
            {
                array: [-5,4,0,-1],
                expected: [-5,-1,0,4],
            },
        ];

        testData.forEach(data => {
            const {array, expected} = data;

            const actual = SelectionSort(array);

            it(`Вводим:  [${array}], Должно вернуть: ${expected} `, () => {
                assert.deepEqual(actual, expected);
            });
        });
    });

    describe('InsertionSort', () => {
        const testData = [
            {
                array: [1,6,4,2],
                expected: [1,2,4,6],
            },
            {
                array: [4],
                expected: [4],
            },
            {
                array: [0,2,-5,7],
                expected: [-5,0,2,7],
            },
            {
                array: [-5,4,0,-1],
                expected: [-5,-1,0,4],
            },
        ];

        testData.forEach(data => {
            const {array, expected} = data;

            const actual = InsertionSort(array);

            it(`Вводим:  [${array}], Должно вернуть: ${expected} `, () => {
                assert.deepEqual(actual, expected);
            });
        });
    });

    describe('dayOfTheWeek', () => {
        const testData = [
            {
                day: 1,
                expected:"Sunday",
            },
            {
                day: 2,
                expected: "Monday",
            },
            {
                day: 7,
                expected: "Saturday",
            },
            {
                day: 9,
                expected: "Введите корректное число",
            },
            {
                day: -5,
                expected: "Введите корректное число",
            },
            {
                day: 0,
                expected: "Введите корректное число",
            },
        ];

        testData.forEach(data => {
            const {day, expected} = data;

            const actual = dayOfTheWeek(day);

            it(`Вводим: ${day}, Должно вернуть: ${expected} `, () => {
                assert.strictEqual(actual, expected);
            });
        });
    });





});
    // describe('getQuarterNumber', () => {
    //     const testData = [
    //         {
    //             x: 1,
    //             y: 2,
    //             expected: 1,
    //         },
    //         {
    //             x: -2,
    //             y: 3,
    //             expected: 2,
    //         },
    //         {
    //             x: -2,
    //             y: -3,
    //             expected: 3,
    //         },
    //         {
    //             x: 3,
    //             y: -3,
    //             expected: 4
    //         },
    //         {
    //             x: 2,
    //             y: 0,
    //             expected: -1,
    //         },
    //         {
    //             x: 0,
    //             y: 2,
    //             expected: -1,
    //         },
    //         {
    //             x: 0,
    //             y: 0,
    //             expected: -1,
    //         }
    //     ];
    //     testData.forEach(data => {
    //         const {x, y, expected} = data;
    //
    //         const actual = getQuarterNumber(x, y);
    //         it(`Should return quarter ${expected} when x = ${x} and y = ${y}`, () => {
    //             assert.strictEqual(actual, expected);
    //         });
    //     });
    // });