class Node {
    constructor(value) {
        this.value = value;
        this.next = null;
        this.prev = null;
    }
}

class LinkedList {
    constructor() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    size() {
        return this.length;
    }

    unshift(value) {
        const newNode = new Node(value);
        newNode.next = this.head;

        if (this.head) {
            this.head.preb = newNode;
        } else {
            this.tail = newNode;
        }

        this.head = newNode;
        this.length++;
    }

    push(value) {
        const newNode = new Node(value);
        newNode.tail = this.tail;

        if (this.tail) {
            this.tail.next = newNode;
        } else {
            this.head = newNode;
        }
        this.tail = newNode;
        this.length++;
    }

    shift() {
        if (!this.head) {
            return null;
        }
        let value = this.head.value;
        this.head = this.head.next;

        if (this.head) {
            this.head.prev = null;
        } else {
            this.tail = null;
        }
        this.length--;
        return value;
    }

    pop() {
        if (!this.tail) {
            return null;
        }
        let value = this.tail.value;
        this.tail = this.tail.prev;

        if (this.tail) {
            this.tail.next = null;
        } else {
           this.tail = null;
        }
        this.length--;
        return value;
    }

    print() {
        const result = [];

        let current = this.head;
        while (current) {
            result.push(current.value);
            current = current.next;
        }
        return result;
    }


}