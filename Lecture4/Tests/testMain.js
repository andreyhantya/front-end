describe("push", function () {
    it("Проверка на несколько положительных чисел", function () {
        const array = new AList([1, 2, 3]);

        let actual = array.push(4, 5);

        let expected = [1, 2, 3, 4, 5];
        assert.deepEqual(actual, expected);
    });

    it("Проверка на одно число", function () {
        const array = new AList([1, 2, 3]);

        let actual = array.push(1);

        let expected = [1, 2, 3, 1];
        assert.deepEqual(actual, expected);
    });

    it("Проверка на отрицательные числа и ноль", function () {
        const array = new AList([1, 2, 3]);

        let actual = array.push(-5, -4, 0);

        let expected = [1, 2, 3, -5, -4, 0];
        assert.deepEqual(actual, expected);
    });
});

describe("pop", function () {
    it("Проверка на возврат значения", function () {
        const array = new AList([1, 2, 3]);

        let actual = array.pop();

        let expected = 3;
        assert.deepEqual(actual, expected);
    });

    it("Проверка на изменение массива", function () {
        const array = new AList([1, 2, 3]);
        array.pop();

        let actual = array.store;

        let expected = [1, 2];
        assert.deepEqual(actual, expected);
    });

    it("Проверка на возвращение Undefined", function () {
        const array = new AList([]);

        let actual = array.pop();

        let expected = undefined;
        assert.deepEqual(actual, expected);
    });

});

describe("shift", function () {
    it("Проверка на возвращаемый елемент", function () {
        const array = new AList([1, 2, 3]);

        let actual = array.shift();

        let expected = 1;
        assert.deepEqual(actual, expected);
    });

    it("Проверка на изменение массива", function () {
        const array = new AList([1, 2, 3]);
        array.shift();

        let actual = array.store;

        let expected = [2, 3];
        assert.deepEqual(actual, expected);
    });

    it("Проверка на возвращение строкового елемента", function () {
        const array = new AList(["Привет", 2, 3]);

        let actual = array.shift();

        let expected = "Привет";
        assert.deepEqual(actual, expected);
    });

    it("Проверка удаление строкового елемента из массива", function () {
        const array = new AList(["Привет", 2, 3]);
        array.shift();

        let actual = array.store;

        let expected = [2, 3];
        assert.deepEqual(actual, expected);
    });

});

describe("unshift", function () {
    it("Проверка на возврат значения", function () {
        const array = new AList([1, 2, 3]);

        let actual = array.unshift(2);

        let expected = 4;
        assert.deepEqual(actual, expected);
    });

    it("Проверка содержимого массива после добавления", function () {
        const array = new AList([2, 3]);
        array.unshift("Привет");

        let actual = array.store;

        let expected = ["Привет", 2, 3];
        assert.deepEqual(actual, expected);
    });

    it("Проверка на возврат значения после добавления нескольких елементов", function () {
        const array = new AList([1, 2, 3]);

        let actual = array.unshift("Привет", 23, -5);

        let expected = 6;
        assert.deepEqual(actual, expected);
    });

    it("Проверка на изменение содержимого массива при добавлении нескольких елементов", function () {
        const array = new AList([1, 2, 3]);
            array.unshift("Привет",2,5);

        let actual = array.store;

        let expected = ["Привет", 2, 5, 1, 2, 3];
        assert.deepEqual(actual, expected);
    });
});

describe("sort", function () {
    it("Сортировка положительных чисел", function () {
        const array = new AList([45, 22, 7, 1, 0]);

        let actual = array.sort();

        let expected = [0, 1, 7, 22, 45];
        assert.deepEqual(actual, expected);
    });

    it("Сорировка отрицательных и положительных чисел", function () {
        const array = new AList([-2, -5, 0, 3]);

        let actual = array.sort();

        let expected = [-5, -2, 0, 3];
        assert.deepEqual(actual, expected);
    });

    it("Сортировка только отрицательных чисел", function () {
        const array = new AList([-3, -10, -5]);

        let actual = array.sort();

        let expected = [-10, -5, -3];
        assert.deepEqual(actual, expected);
    });
});

describe("toString", function () {
    it("Перевод массива в строку", function () {
        const array = new AList([45, 22, 7, 1, 0]);

        let actual = array.toString();

        let expected = '"[45, 22, 7, 1, 0]"';
        assert.strictEqual(actual, expected);
    });

    it("Перевод массива в строку с строковыми елементами и отрицательными числами", function () {
        const array = new AList([45, "hello", -7, 1, 0]);

        let actual = array.toString();

        let expected = '"[45, hello, -7, 1, 0]"';
        assert.deepEqual(actual, expected);
    });


});