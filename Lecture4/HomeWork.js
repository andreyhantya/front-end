class AList {


    constructor(store) {
        this.store = store;
        if (store === undefined) {
            return [];
        }
    }

//***********************************
    pop() {
        if (this.store === undefined) {
            this.store = [];
        }
        if (this.store.length === 0) {
            return undefined
        } else {

            let newArray = [];
            let storeLength = this.store.length - 1;
            let lastElement = this.store[storeLength];

            for (let i = 0; i < storeLength; i++) {
                newArray[i] = this.store[i];
            }

            this.store = newArray;
            return lastElement;
        }
    }

//***********************************
    push() {
        if (this.store === undefined) {
            this.store = [];
        }
        let argLength = arguments.length;
        let count = 0;

        argLength += this.store.length;

        for (let i = this.store.length; i < argLength; i++) {
            this.store[i] = arguments[count];
            count++;
        }

        return this.store;
    }

//***********************************
//удаляет из массива первый елемент и возвращает его
    shift() {
        if (this.store === undefined) {
            this.store = [];
        }
        let newArray = [];
        let firstElement = this.store[0];
        let storeLength = this.store.length;
        let count = 0;

        for (let i = 1; i < storeLength; i++) {
            newArray[count] = this.store[i];
            count++;
        }

        this.store = newArray;
        return firstElement;
    }

//***********************************
    unshift() {
        if (this.store === undefined) {
            this.store = [];
        }
        let newArray = [];
        let storeLength = this.store.length;
        let argLength = arguments.length;
        let count = 0;

        for (let i = 0; i < argLength; i++) {
            newArray[i] = arguments[i];
        }

        for (let i = argLength; i < argLength + storeLength; i++) {
            newArray[i] = this.store[count];
            count++;
        }

        this.store = [];
        this.store = newArray;

        return this.store.length;

    }

//***********************************
    sort = () => {
        let arrayLength = this.store.length;
        let array = this.store;

        for (let i = 0; i < arrayLength - 1; i++) {
            for (let j = 0; j < arrayLength - 1 - i; j++) {

                if (array[j + 1] < array[j]) {
                    let temp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = temp;
                }
            }
        }

        return array;
    };
//***********************************

    toString = () => {
        let string = '"[';
        let array = this.store;

        for (let i = 0; i < array.length; i++) {
            string += `${array[i]}`;
            if (i !== array.length - 1) {
                string += ', ';
            }
        }

        string += ']"';

        return string;
    };


}

let q = new AList([10, 5, 8]);
